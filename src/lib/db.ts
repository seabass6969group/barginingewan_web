import { Model, Schema, SchemaType, connect, model } from "mongoose";
import dotenv from "dotenv"

dotenv.config()
export async function connectDB() {

    if (process.env.ISPROD == "DEV") {
        await connect(process.env.DEV_MONGOURL)
    } else if (process.env.ISPROD == "PROD") {
        await connect(process.env.PROD_MONGOURL)
    }
}
const userSchema = new Schema({
    name: String,
    userID: String,
    score: Number,
    MinecraftUUID: String,
    ownedCards: [{type: String}]
});

export const User = model("user", userSchema)

const interestRateSchema = new Schema({
    interestRate: Number,
    date: Date
})
export const InterestRate = model("interestRate", interestRateSchema)


const LoggingSchema = new Schema({
    activity: String,
    date: Date,
    userID: String
})
export const Logging = model("log", LoggingSchema)
const eventSchema = new Schema({
    userEffect: String,
    expiryDate: Date,
    userCreator: String,
    userType: String,
    eventType: Schema.Types.Mixed
})
export const event = model("event", eventSchema)
const cardInventorySchema = new Schema({
    cardsId: String,
    cardsRarity: String,
    cardsType: String,
    inventory: Number,
    imageUrl: String
})
export const cardInventory = model("card", cardInventorySchema)