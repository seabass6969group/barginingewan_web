import { User, event } from "$lib/db"
import { json, type RequestHandler } from "@sveltejs/kit"

export const POST: RequestHandler = async ({ request, cookies }) => {
    const searcher = await request.json()
    if (searcher.TOKEN != process.env.ACCESS_TOKEN) { return json({ error: "unauthorized access" }, { status: 401 }) }
    if (
        searcher.ID!= undefined
    ) {
        event.deleteOne({_id: searcher.ID})
        return json({"test": "deleted"}, {status: 201})
    } else {
        return json({ error: "error" }, { status: 404 })
    }
}