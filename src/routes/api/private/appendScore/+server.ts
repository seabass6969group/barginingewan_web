import { User } from "$lib/db"
import { json, type RequestHandler } from "@sveltejs/kit"

export const POST: RequestHandler = async ({ request, cookies }) => {
    const searcher = await request.json()
    if(searcher.TOKEN != process.env.ACCESS_TOKEN){ return json({error: "unauthorized access"}, {status: 401}) }
    if(searcher.userID != undefined && searcher.score != undefined){
        const user = await User.findOne({userID: searcher.userID}, "score")
        if(user != null){
            await User.updateOne({userID: searcher.userID}, {score: user.score + searcher.score})
            const NewUser = await User.findOne({userID: searcher.userID}, "score")
            return json({"success": NewUser.score}, {status: 201})
        }
        return json({error: "error"}, {status: 404})
    }else{
        return json({error: "error"}, {status: 404})
    }
}