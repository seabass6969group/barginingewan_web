import { User } from "$lib/db"
import { json, type RequestHandler } from "@sveltejs/kit"

export const POST: RequestHandler = async ({ request, cookies }) => {
    
    const body = await request.json()
    if(body.TOKEN != process.env.ACCESS_TOKEN){ return json({error: "unauthorized access"}, {status: 401}) }
    Reflect.deleteProperty(body, "TOKEN")
    const userSch = new User(body)
    userSch.validateSync()
    userSch.save()
    return json({"test": body}, {status: 201})
}