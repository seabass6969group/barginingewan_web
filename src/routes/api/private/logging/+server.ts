import { Logging } from "$lib/db"
import { json, type RequestHandler } from "@sveltejs/kit"

export const POST: RequestHandler = async ({ request, cookies }) => {
    const searcher = await request.json()
    if(searcher.TOKEN != process.env.ACCESS_TOKEN){ return json({error: "unauthorized access"}, {status: 401}) }
    if(searcher.userID != undefined && searcher.activity != undefined){
        const log = new Logging({activity: searcher.acitivity, userID: searcher.userID, date: new Date()})
        log.validateSync()
        log.save()
        return json({"done": "success"}, {status: 201})
    }else{
        return json({error: "error"}, {status: 404})
    }
}