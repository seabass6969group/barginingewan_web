import { User } from "$lib/db"
import { json, type RequestHandler } from "@sveltejs/kit"

export const POST: RequestHandler = async ({ request, cookies }) => {
    const searcher = await request.json()
    if(searcher.TOKEN != process.env.ACCESS_TOKEN){ return json({error: "unauthorized access"}, {status: 401}) }
    if(searcher.userID != undefined){
        const count = await User.where({userID: searcher.userID}).countDocuments()
        if(count > 0){
            return json({"exist": "yes"}, {status: 201})
        }else{
            return json({"exist": "no"}, {status: 404})
        }

    }else{
        return json({error: "error"}, {status: 404})
    }
}