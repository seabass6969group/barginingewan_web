import { User } from "$lib/db"
import { json, type RequestHandler } from "@sveltejs/kit"

export const POST: RequestHandler = async ({ request, cookies }) => {
    const searcher = await request.json()
    if(searcher.TOKEN != process.env.ACCESS_TOKEN){ return json({error: "unauthorized access"}, {status: 401}) }
    if(searcher.userID != undefined){
        const user = await User.deleteOne({userID: searcher.userID})
        if(user != null){
            return json({"success": "deleted"}, {status: 201})
        }
        return json({error: "error"}, {status: 404})
    }else{
        return json({error: "error"}, {status: 404})
    }
}