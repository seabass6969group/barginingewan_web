import { cards } from "$lib/cards"
import { User, cardInventory } from "$lib/db"
import { json, type RequestHandler } from "@sveltejs/kit"
export const POST: RequestHandler = async ({ request, cookies }) => {
    const searcher = await request.json()
    if (searcher.TOKEN != process.env.ACCESS_TOKEN) { return json({ error: "unauthorized access" }, { status: 401 }) }
    if (searcher.id != undefined && searcher.userID != undefined) {
        const finder = await cardInventory.findOne({ cardsId: searcher.id }, "inventory")
        if (finder != null) {
            await cardInventory.updateOne({ cardsId: searcher.id }, { inventory: finder.inventory - 1 })
            const find = await User.findOne({userID: searcher.userID})
            find?.ownedCards.push(JSON.stringify(cards.find(t => t.id == searcher.id)))
            find?.save()
            return json({ "done": "done" })
        } else {
            return json({ "failed": "failed" })
        }
    } else {
        return json({ "failed": "failed" })
    }
}