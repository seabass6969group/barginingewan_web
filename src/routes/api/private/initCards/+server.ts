import { cards, type rarity } from "$lib/cards";
import { cardInventory } from "$lib/db"
import { json, type RequestHandler } from "@sveltejs/kit"
function rarityToInventory(rarity: rarity){
    switch (rarity) {
        case "Common":
            return 1000
            break;
    
        case "Uncommon":
            return 500
            break;
    
        case "Rare":
            return 100
            break;
    
        case "Legendary":
            return 50
            break;
    
        case "Mythic":
            return 20
            break;
        case "Godly":
            return 3
            break;
    
    }
}
export const POST: RequestHandler = async ({ request, cookies }) => {
    // const searcher = await request.json()
    // if(searcher.TOKEN != process.env.ACCESS_TOKEN){ return json({error: "unauthorized access"}, {status: 401}) }
    // cards.forEach(element => {
    //     const card = new cardInventory({
    //         cardsId: element.id,
    //         cardsRarity: element.rarity,
    //         inventory: rarityToInventory(element.rarity),
    //         imageUrl: element.image_url,
    //         cardsType: element.card_type
    //     })
    //     card.validateSync()
    //     card.save()
    // });
    return json({"error": "notInUseAnymore"})
}