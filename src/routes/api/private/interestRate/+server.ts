import { InterestRate, User } from "$lib/db"
import { json, type RequestHandler } from "@sveltejs/kit"

export const POST: RequestHandler = async ({ request, cookies }) => {
    const searcher = await request.json()
    if(searcher.TOKEN != process.env.ACCESS_TOKEN){ return json({error: "unauthorized access"}, {status: 401}) }
    if(searcher.interest != undefined){
        const allUser = await User.find({}, ["userID", "score"])
        allUser.forEach(async user => {
            await user.updateOne({$inc: {score: Math.floor(user.score * searcher.interest)}})
            await user.save()
        })
        const newInterest = new InterestRate({interestRate: searcher.interest, date: new Date()})
            await newInterest.validateSync()
            await newInterest.save()
        return json({"done": "success"}, {status: 201})
    }else{
        return json({error: "error"}, {status: 404})
    }
}