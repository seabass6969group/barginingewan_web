import { cardInventory } from "$lib/db"
import { json, type RequestHandler } from "@sveltejs/kit"
export const POST: RequestHandler = async ({ request, cookies }) => {
    const searcher = await request.json()
    if (searcher.TOKEN != process.env.ACCESS_TOKEN) { return json({ error: "unauthorized access" }, { status: 401 }) }
    if (searcher.cardsType instanceof Array) {
        const searcherType = searcher.cardsType
        const search = searcherType as string[]
        let searching: {$or: {cardsType: string}[], inventory: {$gt: number}} = { $or: [], inventory: {$gt: 0}}
        search.forEach((items) => {
            searching.$or.push({ cardsType: items })
        })
        const inventory = await cardInventory.find(searching, {})
        return json({ "inventory": inventory }, { status: 201 })
    } else {
        return json({ error: "Not a valid type" }, { status: 401 })
    }

}