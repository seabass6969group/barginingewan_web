import { InterestRate } from "$lib/db"
import { json, type RequestHandler } from "@sveltejs/kit"

export const POST: RequestHandler = async ({ request, cookies }) => {
    if(searcher.TOKEN != process.env.ACCESS_TOKEN){ return json({error: "unauthorized access"}, {status: 401}) }
    const allInterestHistory = await InterestRate.find({})
    return json({history: allInterestHistory.sort((a, b) => b.date?.getTime() - a.date?.getTime())}, {status: 201})
}