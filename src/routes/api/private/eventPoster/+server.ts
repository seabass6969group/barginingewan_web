import { User, event } from "$lib/db"
import { json, type RequestHandler } from "@sveltejs/kit"

export const POST: RequestHandler = async ({ request, cookies }) => {
    const searcher = await request.json()
    if (searcher.TOKEN != process.env.ACCESS_TOKEN) { return json({ error: "unauthorized access" }, { status: 401 }) }
    if (
        searcher.userEffect != undefined &&
        searcher.expiryDate != undefined &&
        searcher.userCreator != undefined &&
        searcher.eventType != undefined &&
        searcher.eventInfo != undefined
    ) {
        const items = new event({searcher})
        items.save()
        items.validateSync()
        return json({"test": items}, {status: 201})
    } else {
        return json({ error: "error" }, { status: 404 })
    }
}