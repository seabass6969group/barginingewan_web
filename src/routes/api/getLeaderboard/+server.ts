import { User } from "$lib/db"
import { json, type RequestHandler } from "@sveltejs/kit"

export const GET: RequestHandler = async ({ request, cookies }) => {
    const allItem = (await User.find({}, ["score", "name", "userID"]))
    return json({"test": allItem.sort((a,b) => b.score-a.score)}, {status: 201})
}