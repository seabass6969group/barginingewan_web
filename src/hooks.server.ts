import { connectDB } from "$lib/db";
import type { Handle } from '@sveltejs/kit';
export const handle: Handle = async ({ event, resolve }) => {
	const response = await resolve(event);
	if (event.url.pathname.startsWith('/api/private/')) {
		// if(response.headers.get("X-access-token") == process.env.ACCESS_TOKEN){
			return response;
		// }else{
		// 	return new Response('You are not allow to access this')
		// }
	}
	return response;
};
connectDB()