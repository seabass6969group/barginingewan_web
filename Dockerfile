FROM ubuntu:latest
ENV WORKDIR "/work"
ENV ACCESS_TOKEN "secrets"
WORKDIR ${WORKDIR}
RUN apt-get update -y
RUN apt-get install -y curl
RUN curl -fsSL https://deb.nodesource.com/setup_21.x | bash - && apt install -y nodejs
COPY . /work
RUN npm install
EXPOSE 3000
RUN npm run build
CMD npm start


