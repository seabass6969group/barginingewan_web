FROM ubuntu:latest
ENV WORKDIR "/work"
COPY . /work
WORKDIR ${WORKDIR}
RUN apt-get update -y
RUN apt-get install -y curl
RUN curl -fsSL https://deb.nodesource.com/setup_21.x | bash - && apt install -y nodejs
RUN npm install
EXPOSE 3000
CMD npm run dev -- --host


